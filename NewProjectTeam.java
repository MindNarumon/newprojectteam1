/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package newprojectteam;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 *
 * @author acer
 */
public class NewProjectTeam {

      static Scanner kb = new Scanner(System.in);
	
	static String rec_username;
	static String rec_password;
	static boolean foundID = false, foundPass = false;
        static ArrayList<Person> arr = new ArrayList<>();
        
        static String a1, a2, a3, a4, e1, e2, e3, e4;
	static double a5, a6, e5, e6;

        static Person wait;
	static boolean foundEditPass = false;

        
        public static void main(String[] args) {
		load();
		sayLogin();
	}
public static void load() {
		arr.add(new Person("admin", "1234", "kevin", "kyle", 178, 75));
		arr.add(new Person("mind", "4567", "narumon", "yujaroen", 158, 38));
	}


        
        public static void sayLogin() {
		System.out.println("Please input your username and password");
		System.out.print("Username : ");
		rec_username = kb.next();
		System.out.print("Password : ");
		rec_password = kb.next();
		checkLogin();
	}

	public static void checkLogin() {
		for (Person a : arr) {
			if (rec_username.equals(a.getUsername())) {
				foundID = true;
				if (rec_password.equals(a.getPassword())) {
					foundPass = true;
					break;
				} else {
					foundPass = false;
					break;
				}
			} else {
				foundID = false;
			}
		}
		checkLoginFound();
	}

	public static void checkLoginFound() {
		if (foundID == true && foundPass == true) {
			showSuccess();
			sayMenu();
		} else if (foundID == true && foundPass == false) {
			showErrorLoginPassword();
			sayLogin();
		} else {
			showErrorLoginUsername();
			sayLogin();
		}
	}

    private static void showSuccess() {
        System.out.println("Success.");
    }

    private static void showErrorLoginPassword() {
        System.err.println("Password is incorrect.");
    }

    private static void showErrorLoginUsername() {
        System.err.println("User is incorrect.");
    }
 private static void sayMenu() {
    System.out.println("Menu Panel");
		System.out.println("1. Add");
		System.out.println("2. Show");
		System.out.println("3. Edit");
		System.out.println("4. Log Out");
		System.out.println("5. Exit");
		System.out.println("");
		checkMenu();
    }
 public static void checkMenu() {
		showPleaseChoose();
		String input = kb.next();
		switch (input) {
		case "1":
			sayAdd();
			break;
		case "2":
			sayShow();
			break;
		case "3":
			sayEditCheckPass();
			break;
		case "4":
			logOut();
			break;
		case "5":
			System.exit(0);
			break;
		default:
			showErrorInput();
			sayMenu();
		}
	}

    private static void showPleaseChoose() {
        System.out.print("Please choose : ");
    }

    private static void sayAdd() {
        System.out.println("Please input information");
		System.out.println("1. Username 2. Password 3. Firstname 4. Lastname 5. Weight 6. Height");
		System.out.println("(0) back");
		add();

    }
private static void add() {
        chooseAddUser();
		chooseAddPass();
		chooseAddFname();
		chooseAddLname();
		chooseAddWeight();
		chooseAddHeight();
		addSave();
    }
public static void chooseAddUser() {
		System.out.print("username : ");
		a1 = kb.next();
		if (a1.equals("0")) {
			sayMenu();
		} else {
			for (Person a : arr) {
				if (a.getUsername().equals(a1)) {
					showErrorAddUser();
					chooseAddUser();
				}
			}
			System.out.println("Username can use.");
		}

	}

	public static void chooseAddWeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("weight (cannot be zero) : ");
				a5 = kb.nextDouble();
				if (a5 == 0) {
					sayMenu();
					break;
				}
				check = true;
			} catch (InputMismatchException e) {
				showErrorWeight();
				kb.next();
			}
		}
		System.out.println("Weight can use.");
	}

	public static void chooseAddHeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("height (cannot be zero) : ");
				a6 = kb.nextDouble();
				if (a6 == 0) {
					sayMenu();
					break;
				}
				check = true;
			} catch (InputMismatchException e) {
				showErrorHeight();
				kb.next();
			}
		}
		System.out.println("Height can use.");
	}

	public static void chooseAddPass() {
		System.out.print("password : ");
		a2 = kb.next();
		if (a2.equals("0"))
			sayMenu();
	}

	public static void chooseAddFname() {
		System.out.print("firstname : ");
		a3 = kb.next();
		if (a3.equals("0"))
			sayMenu();
	}

	public static void chooseAddLname() {
		System.out.print("lastname : ");
		a4 = kb.next();
		if (a4.equals("0"))
			sayMenu();
	}
        private static void addSave() {
               System.out.println("(0) back / (Y) save");
		showPleaseChoose();
		String input = kb.next();
		if (input.equalsIgnoreCase("Y")) {
			arr.add(new Person(a1, a2, a3, a4, a5, a6));
			showSuccess();
			sayMenu();
		} else if (input.equals("0")) {
			sayAdd();
		} else
			addSave();

        }
        public static void addSetDefault() {
		a1 = "";
		a2 = "";
		a3 = "";
		a4 = "";
		a5 = 0;
		a6 = 0;
	}


    private static void showErrorInput() {
        System.err.println("Input didn't match.");
    }
    private static void showErrorHeight() {
        System.err.println("Height  must be number.");
    }

    private static void showErrorAddUser() {
        System.err.println("If Username has been exist. This message will show.");
    }

    private static void showErrorWeight() {
        System.err.println("Weight must be number.");
    }

    private static void sayShow() {
    System.out.println("Show Panel");
		for (int i = 0; i < arr.size(); i++) {
			System.out.println(i + 1 + ". " + arr.get(i).Username);
		}
		System.out.println("(0) back");
		show();

    }
    public static void show() {
		boolean check = false;
		int input;
		while (!check) {
			try {
				showPleaseChoose();
				input = kb.nextInt();
				checkShow(input);

			} catch (InputMismatchException e) {
				showErrorShowInput();
				kb.next();
			}
		}
	}
public static void checkShow(int input) {
		for (int i = 0; i < arr.size(); i++) {
			if (input == i + 1) {
				System.out.println(arr.get(i));
				sayShow();
				break;
			} else if (input == 0) {
				sayMenu();
				break;
			} else if (input > arr.size()) {
				showErrorInput();
				sayShow();
				break;
			}
		}
		showErrorInput();
		sayShow();
	}

public static void showErrorShowInput() {
		System.err.println("Weight must be number.");
	}

public static void sayEditCheckPass() {
		editCheckPass();
		showPleaseChoose();
		String input = kb.next();
		if (input.equals("0")) {
			sayMenu();
		} else {
			editPassFound(input);
		}
	}

	public static void editCheckPass() {
		System.out.println("Please input your password");
		System.out.println("(0) back");
	}

	public static void editPassFound(String input) {
		for (Person a : arr) {
			if (input.equals(a.getPassword()) && input.equals(rec_password)) {
				wait = new Person(a.getUsername(), a.getPassword(), a.getFirst(), a.getLast(), a.getWeight(), a.getHeight());
				sayEdit();
			}
		}
		if (foundEditPass == false) {
			showErrorEditPass();
			sayEditCheckPass();
		}
	}

	public static void sayEdit() {
		chooseEdit();
		String input = kb.next();
		if (input.equals("0")) {
			sayMenu();
		} else if (input.equals("2")) {
			chooseEditPass();
			sayEdit();
		} else if (input.equals("3")) {
			chooseEditFname();
			sayEdit();
		} else if (input.equals("4")) {
			chooseEditLname();
			sayEdit();
		} else if (input.equals("5")) {
			chooseEditWeight();
			sayEdit();
		} else if (input.equals("6")) {
			chooseEditHeight();
			sayEdit();
		} else if (input.equalsIgnoreCase("Y")) {
			editSave();
		} else {
			showErrorInput();
			sayEdit();
		}

	}
public static void chooseEdit() {
		System.out.println("Edit Panel");
		System.out.println(
				"1. Username: " + wait.getUsername() + " 2. Password: " + wait.getPassword() + " 3. Firstname: " + wait.getFirst()
						+ " 4. Lastname: " + wait.getLast() + " 5. Weight: " + wait.getWeight() + " 6. Height: " + wait.getHeight());
		System.out.println("(0) back / (Y)save");
		System.err.println("Username can't change!");
	}

	public static void editSave() {
		for (Person a : arr) {
			if (a.getUsername().equals(wait.Username)) {
				a.setPassword(wait.getPassword());
				a.setFirst(wait.getFirst());
				a.setLast(wait.getLast());
				a.setWeight(wait.getWeight());
				a.setHeight(wait.getHeight());
			}
		}
		showSuccess();
		sayMenu();
	}

	public static void chooseEditWeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("weight : ");
				e5 = kb.nextDouble();
				if (e5 == 0) {
					sayEdit();
					break;
				}
				wait.setWeight(e5);
				check = true;
			} catch (InputMismatchException e) {
				showErrorWeight();
				kb.next();
			}
		}
		System.out.println("Weight can use.");
	}

	public static void chooseEditHeight() {
		boolean check = false;
		while (!check) {
			try {
				System.out.print("height : ");
				e6 = kb.nextDouble();
				if (e6 == 0) {
					sayEdit();
					break;
				}
				wait.setHeight(e6);
				check = true;
			} catch (InputMismatchException e) {
				showErrorHeight();
				kb.next();
			}
		}
		System.out.println("Height can use.");
	}

	public static void chooseEditPass() {
		System.out.print("password : ");
		e2 = kb.next();
		if (e2.equals("0"))
			sayEdit();
		wait.setPassword(e2);
	}

	public static void chooseEditFname() {
		System.out.print("firstname : ");
		e3 = kb.next();
		if (e3.equals("0"))
			sayEdit();
		wait.setFirst(e3);
	}

	public static void chooseEditLname() {
		System.out.print("lastname : ");
		e4 = kb.next();
		if (e4.equals("0"))
			sayEdit();
		wait.setLast(e4);
	}
        public static void showErrorEditPass() {
		System.out.println("password didn't match");
	}


	public static void logOut() {
		System.out.println("Do you want to logout?(Y/N)");
		showPleaseChoose();
		String input = kb.next();
		if (input.equalsIgnoreCase("Y")) {
			sayLogin();
		} else if (input.equalsIgnoreCase("N")) {
			sayMenu();
		} else
			logOut();

	}



   
}






class Person {
    
    String Username;
    String Password;
    String Firstname;
    String Lastname;
    double Weight;
    double Height;
    
Person(String Username,String Password,String Firstname,String Lastname,double Weight,double Height) {
    this.Username = Username;
    this.Password = Password;
    this.Firstname = Firstname;
    this.Lastname = Lastname;
    this.Weight = Weight;
    this.Height = Height;
    }   
public String toString() {
		String p1 = "Username : " + Username;
		String p2 = "\nPassword : " + Password;
		String p3 = "\nFirstname : " + Firstname;
		String p4 = "\nLastname : " + Lastname;
		String p5 = "\nWeight : " + Double.toString(Weight);
		String p6 = "\nHeight : " + Double.toString(Height);
		return p1 + p2 + p3 + p4 + p5 + p6;

	}


public String getUsername() {
    return Username;
}
public String getPassword() {
    return Password;
}
public String getFirst() {
    return Firstname;
}
public String getLast() {
    return Lastname;
}
public Double getWeight() {
    return Weight;
}public Double getHeight() {
    return Height;
}
public void setPassword(String password) {
		this.Password = Password;
	}

	public void setFirst(String Firstname) {
		this.Firstname = Firstname;
	}

	public void setLast(String Lastname) {
		this.Lastname = Lastname;

	}

	public void setWeight(double Weight) {
		this.Weight = Weight;
	}

	public void setHeight(double Height) {
		this.Height = Height;
	}

}